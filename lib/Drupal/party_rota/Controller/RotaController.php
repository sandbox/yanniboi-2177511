<?php

/**
 * @file
 * Contains \Drupal\party_rota\Controller\RotaController.
 */

namespace Drupal\party_rota\Controller;

use Drupal\Component\Utility\String;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\ConfirmFormBase;

/**
 * Returns responses for Node routes.
 */
class RotaController extends ControllerBase {

  /**
   * Provides the node submission form.
   *
   * @return array
   *   A node submission form.
   */
  public function add() {
    $account = $this->currentUser();
    $langcode = $this->moduleHandler()->invoke('language', 'get_default_langcode', array('rota', 'rota'));

    $rota = $this->entityManager()->getStorageController('rota')->create(array(
      'langcode' => $langcode ? $langcode : $this->languageManager()->getCurrentLanguage()->id,
    ));

    $form = $this->entityManager()->getForm($rota);
    return $form;
  }

  /**
   * Provides the node submission form.
   */
  public function assign(ContentEntityInterface $rota, ContentEntityInterface $rota_position, ContentEntityInterface $rota_slot) {
    $account = $this->currentUser();
    $langcode = $this->moduleHandler()->invoke('language', 'get_default_langcode', array('rota', 'rota'));

    // Add the field related form elements.
    $rota = $this->entity;

    $form = array();
    $form_state = array();

    $form_state['rota'] = $rota;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('<em>Edit</em> @title', array('@title' => $rota->label()));
    }

    $form['#attributes']['class'][0] = drupal_html_class('rota-assign-form');

    return ConfirmFormBase::buildForm($form, $form_state);
  }

  /**
   * Displays a node.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $rota
   *   The node we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(ContentEntityInterface $rota) {
    $build = $this->buildPage($rota);

    foreach ($rota->uriRelationships() as $rel) {
      $uri = $rota->uri($rel);
      // Set the node path as the canonical URL to prevent duplicate content.
      $build['#attached']['drupal_add_html_head_link'][] = array(
        array(
          'rel' => $rel,
          'href' => $this->urlGenerator()->generateFromPath($uri['path'], $uri['options']),
        )
      , TRUE);

      if ($rel == 'canonical') {
        // Set the non-aliased canonical path as a default shortlink.
        $build['#attached']['drupal_add_html_head_link'][] = array(
          array(
            'rel' => 'shortlink',
            'href' => $this->urlGenerator()->generateFromPath($uri['path'], array_merge($uri['options'], array('alias' => TRUE))),
          )
        , TRUE);
      }
    }

    return $build;
  }

  /**
   * The _title_callback for the node.view route.
   *
   * @param ContentEntityInterface $rota
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(ContentEntityInterface $rota) {
    return String::checkPlain($this->entityManager()->getTranslationFromContext($rota)->label());
  }

  /**
   * Builds a node page render array.
   *
   * @param ContentEntityInterface $rota
   *   The node we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  protected function buildPage(ContentEntityInterface $rota) {
    return array('rotas' => $this->entityManager()->getViewBuilder('rota')->view($rota));
  }

  /**
   * The _title_callback for the node.add route.
   *
   * @return string
   *   The page title.
   */
  public function addPageTitle() {
    return $this->t('Create Rota');
  }

  public function getQuestion() {
    return t('Are you sure?');
  }

  public function getDescription() {
    return t('Are you sure?');
  }

  public function getFormName() {
    return t('Are you sure?');
  }

  public function getConfirmText() {
    return t('Are you sure?');
  }
}

