<?php

/**
 * @file
 * Contains \Drupal\party_rota\Controller\RotaController.
 */

namespace Drupal\party_rota\Controller;

use Drupal\Component\Utility\String;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\ConfirmFormBase;

/**
 * Returns responses for Node routes.
 */
class SlotController extends ControllerBase {

  /**
   * Provides the node submission form.
   *
   * @return array
   *   A node submission form.
   */
  public function add(ContentEntityInterface $rota) {
    $account = $this->currentUser();
    $langcode = $this->moduleHandler()->invoke('language', 'get_default_langcode', array('slot', 'slot'));

    $slot = $this->entityManager()->getStorageController('rota_slot')->create(array(
      'langcode' => $langcode ? $langcode : $this->languageManager()->getCurrentLanguage()->id,
    ));

    $form = $this->entityManager()->getForm($slot, 'default', array('rota' => $rota));
    return $form;
  }

  /**
   * Displays a node.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $slot
   *   The node we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(ContentEntityInterface $slot) {
    $build = $this->buildPage($slot);

    foreach ($slot->uriRelationships() as $rel) {
      $uri = $slot->uri($rel);
      // Set the node path as the canonical URL to prevent duplicate content.
      $build['#attached']['drupal_add_html_head_link'][] = array(
        array(
          'rel' => $rel,
          'href' => $this->urlGenerator()->generateFromPath($uri['path'], $uri['options']),
        )
      , TRUE);

      if ($rel == 'canonical') {
        // Set the non-aliased canonical path as a default shortlink.
        $build['#attached']['drupal_add_html_head_link'][] = array(
          array(
            'rel' => 'shortlink',
            'href' => $this->urlGenerator()->generateFromPath($uri['path'], array_merge($uri['options'], array('alias' => TRUE))),
          )
        , TRUE);
      }
    }

    return $build;
  }

  /**
   * The _title_callback for the node.view route.
   *
   * @param ContentEntityInterface $slot
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(ContentEntityInterface $slot) {
    return String::checkPlain($this->entityManager()->getTranslationFromContext($slot)->label());
  }

  /**
   * Builds a node page render array.
   *
   * @param ContentEntityInterface $slot
   *   The node we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  protected function buildPage(ContentEntityInterface $slot) {
    return array('slots' => $this->entityManager()->getViewBuilder('slot')->view($slot));
  }

  /**
   * The _title_callback for the node.add route.
   *
   * @return string
   *   The page title.
   */
  public function addPageTitle() {
    return $this->t('Create slot');
  }

  public function getQuestion() {
    return t('Are you sure?');
  }

  public function getDescription() {
    return t('Are you sure?');
  }

  public function getFormName() {
    return t('Are you sure?');
  }

  public function getConfirmText() {
    return t('Are you sure?');
  }
}

