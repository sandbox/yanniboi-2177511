<?php

/**
 * @file
 * Contains \Drupal\party_rota\Controller\RotaController.
 */

namespace Drupal\party_rota\Controller;

use Drupal\Component\Utility\String;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\ConfirmFormBase;

/**
 * Returns responses for Node routes.
 */
class PositionController extends ControllerBase {

  /**
   * Provides the node submission form.
   *
   * @return array
   *   A node submission form.
   */
  public function add(ContentEntityInterface $rota) {
    $account = $this->currentUser();
    $langcode = $this->moduleHandler()->invoke('language', 'get_default_langcode', array('position', 'position'));

    $position = $this->entityManager()->getStorageController('rota_position')->create(array(
      'langcode' => $langcode ? $langcode : $this->languageManager()->getCurrentLanguage()->id,
    ));

    $form = $this->entityManager()->getForm($position, 'default', array('rota' => $rota));
    return $form;
  }

  /**
   * Displays a node.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $position
   *   The node we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(ContentEntityInterface $position) {
    $build = $this->buildPage($position);

    foreach ($position->uriRelationships() as $rel) {
      $uri = $position->uri($rel);
      // Set the node path as the canonical URL to prevent duplicate content.
      $build['#attached']['drupal_add_html_head_link'][] = array(
        array(
          'rel' => $rel,
          'href' => $this->urlGenerator()->generateFromPath($uri['path'], $uri['options']),
        )
      , TRUE);

      if ($rel == 'canonical') {
        // Set the non-aliased canonical path as a default shortlink.
        $build['#attached']['drupal_add_html_head_link'][] = array(
          array(
            'rel' => 'shortlink',
            'href' => $this->urlGenerator()->generateFromPath($uri['path'], array_merge($uri['options'], array('alias' => TRUE))),
          )
        , TRUE);
      }
    }

    return $build;
  }

  /**
   * The _title_callback for the node.view route.
   *
   * @param ContentEntityInterface $position
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(ContentEntityInterface $position) {
    return String::checkPlain($this->entityManager()->getTranslationFromContext($position)->label());
  }

  /**
   * Builds a node page render array.
   *
   * @param ContentEntityInterface $position
   *   The node we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  protected function buildPage(ContentEntityInterface $position) {
    return array('positions' => $this->entityManager()->getViewBuilder('position')->view($position));
  }

  /**
   * The _title_callback for the node.add route.
   *
   * @return string
   *   The page title.
   */
  public function addPageTitle() {
    return $this->t('Create position');
  }

  public function getQuestion() {
    return t('Are you sure?');
  }

  public function getDescription() {
    return t('Are you sure?');
  }

  public function getFormName() {
    return t('Are you sure?');
  }

  public function getConfirmText() {
    return t('Are you sure?');
  }
}

