<?php

/**
 * @file
 * Definition of Drupal\node\NodeFormController.
 */

namespace Drupal\party_rota;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityFormController;
use Drupal\Core\Language\Language;
use Drupal\Component\Utility\String;

/**
 * Form controller for the node edit forms.
 */
class RotaFormController extends ContentEntityFormController {

  /**
   * Default settings for this content/node type.
   *
   * @var array
   */
  protected $settings;

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state) {
    // Add the field related form elements.
    $rota = $this->entity;
    $form_state['rota'] = $rota;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('<em>Edit</em> @title', array('@title' => $rota->label()));
    }

    $form['#attributes']['class'][0] = drupal_html_class('rota-form');

    // Basic node information.
    // These elements are just values so they are not even sent to the client.
    foreach (array('rota_id', 'created') as $key) {
      $form[$key] = array(
        '#type' => 'value',
        '#value' => isset($rota->$key) ? $rota->$key : NULL,
      );
    }

    $form['slots_positions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Slots and Positions'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => $rota->isNew() ? TRUE : FALSE,
    );

    // Get the slots for this rota.
    $slots = $rota->getSlots();

    $form['slots_positions']['slots'] = array(
      '#type' => 'fieldset',
      '#title' => t('Rota Slots'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

        if (!$slots) {
          $form['slots_positions']['slots']['no_slots'] = array(
            '#markup' => t('No slots have been created yet.')
          );

          // Add helper text to tell user that slots can be created after rota is saved.
          if ($rota->isNew()) {
            $form['slots_positions']['slots']['no_slots']['#markup'] .= '</br>' .
              t('Slots can be added after the Rota has been created.');
          }
        }
        else {
          $form_state['slots'] = $slots;

          foreach ($slots as $slot) {
            $form['slots_positions']['slots']['slot_id_' . $slot->id()] = array(
              '#type' => 'markup',
              '#markup' => $slot->label(),
              //'#entity' => $slot,
              //'#theme' => 'party_rota_slot',
            );
          }
        }

        if (!isset($rota->is_new)) {
          /*$add_link = ctools_modal_text_button(
            t('Add Slot'),
            'admin/party_rota/nojs/add/' . $rota->rota_id . '/rota_slot',
            t('Add a Slot to the current Rota'), 'ctools-modal-modal-popup-medium'
          );*/
          $add_link = 'test';

          $form['slots_positions']['slots']['add_link'] =  array(
            '#type' => 'link',
            '#title' => 'Add Slot',
            '#href' => 'party_rota/' . $rota->id() . '/slot/add',
            '#attributes' => array(
              'class' => array('use-ajax'),
              'data-accepts' => 'application/vnd.drupal-modal',
            ),
          );
        }

        // Get the positions for this rota.
        $positions = $rota->getPositions();

        $form['slots_positions']['positions'] = array(
          '#type' => 'fieldset',
          '#title' => t('Rota positions'),
          '#weight' => 1,
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );

        if (!$positions) {
          $form['slots_positions']['positions']['no_positions'] = array(
            '#markup' => t('No positions have been created yet.')
          );

          // Add helper text to tell user that positions can be created after rota is saved.
          if ($rota->isNew()) {
            $form['slots_positions']['positions']['no_positions']['#markup'] .= '</br>' .
              t('Positions can be added after the Rota has been created.');
          }
        }
        else {
          $form_state['positions'] = $positions;

          foreach ($positions as $position) {
            $form['slots_positions']['positions']['position_id_' . $position->id()] = array(
              '#type' => 'markup',
              '#markup' => $position->label(),
              //'#entity' => $position,
              //'#theme' => 'party_rota_position',
            );
          }
        }

        if (!$rota->isNew()) {
          /*$add_link = ctools_modal_text_button(
            t('Add Position'),
            'admin/party_rota/nojs/add/' . $rota->rota_id . '/rota_position',
            t('Add a Position to the current Rota'), 'ctools-modal-modal-popup-medium'
          );*/

          $add_link = 'test';


          $form['slots_positions']['positions']['add_link'] =  array(
            '#type' => 'link',
            '#title' => 'Add Position',
            '#href' => 'party_rota/' . $rota->id() . '/position/add',
            '#attributes' => array(
              'class' => array('use-ajax'),
              'data-accepts' => 'application/vnd.drupal-modal',
            ),
          );
        }

        $form['rota_layout'] = array(
          '#type' => 'fieldset',
          '#title' => t('Rota Layout'),
          '#weight' => 5,
          '#collapsible' => TRUE,
          '#collapsed' => $rota->isNew() ? TRUE : FALSE,
        );

        if (!empty($slots) && !empty($positions)) {
          $header = array(
            'slot' => array('data' => t('Slot')),
          );

          $rows = array();

          foreach ($slots as $sid => $slot) {
            if ($slot->label()) {
              $label = $slot->label() . ' ( ' . date('d/m/Y H:i', $slot->get('date')->value) . ' )';
            }
            else {
              $label = date('d/m/Y H:i', $slot->get('date')->value);
            }
            $rows[$sid] = array(
              'slot' => $label,
            );

            foreach ($positions as $pid => $position) {
              $header[$pid] = array('data' => $position->label());
              if ($assignment = $rota->getAssignmentFromContext($pid, $sid)) {
                /*$delete_link = ctools_modal_text_button(
                  t('Remove'),
                  'admin/party_rota/nojs/delete/' . $assignment->assignment_id,
                  t('Remove assignment and clear slot.'),
                  'ctools-modal-modal-popup-medium'
                );*/

                $delete_link = 'test';

                $rows[$sid][$pid] = $assignment->name . '  (' . $delete_link . ')';
              }
              else {
                /*$add_link = ctools_modal_text_button(
                  t('Add'),
                  'admin/party_rota/nojs/assign/' . $pid . '/' . $sid,
                  t('Add new'),
                  'ctools-modal-modal-popup-medium'
                );*/

                $add_link = 'test';

                $rows[$sid][$pid] = $add_link;
              }
            }
          }

          $form['rota_layout']['table'] = array(
            '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
          );

        }
        else {
          $form['rota_layout']['dock'] = array(
            '#markup' => t('Please make sure this Rota has both Slots and Positions.'),
          );
        }

    return parent::form($form, $form_state, $rota);
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::actions().
   */
  protected function actions(array $form, array &$form_state) {
    $element = parent::actions($form, $form_state);
    $rota = $this->entity;

    $element['submit']['#access'] = TRUE;


    $element['delete']['#access'] = $rota->access('delete');
    $element['delete']['#weight'] = 100;

    return $element;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::validate().
   *
  public function validate(array $form, array &$form_state) {
    $node = $this->buildEntity($form, $form_state);

    if ($node->id() && (node_last_changed($node->id(), $this->getFormLangcode($form_state)) > $node->getChangedTime())) {
      $this->setFormError('changed', $form_state, $this->t('The content on this page has either been modified by another user, or you have already submitted modifications using this form. As a result, your changes cannot be saved.'));
    }

    // Validate the "authored by" field.
    if (!empty($form_state['values']['name']) && !($account = user_load_by_name($form_state['values']['name']))) {
      // The use of empty() is mandatory in the context of usernames
      // as the empty string denotes the anonymous user. In case we
      // are dealing with an anonymous user we set the user ID to 0.
      $this->setFormError('name', $form_state, $this->t('The username %name does not exist.', array('%name' => $form_state['values']['name'])));
    }

    // Validate the "authored on" field.
    // The date element contains the date object.
    $date = $node->date instanceof DrupalDateTime ? $node->date : new DrupalDateTime($node->date);
    if ($date->hasErrors()) {
      $this->setFormError('date', $form_state, $this->t('You have to specify a valid date.'));
    }

    // Invoke hook_node_validate() for validation needed by modules.
    // Can't use module_invoke_all(), because $form_state must
    // be receivable by reference.
    foreach (\Drupal::moduleHandler()->getImplementations('node_validate') as $module) {
      $function = $module . '_node_validate';
      $function($node, $form, $form_state);
    }

    parent::validate($form, $form_state);
  }

  /**
   * Updates the node object by processing the submitted values.
   *
   * This function can be called by a "Next" button of a wizard to update the
   * form state's entity with the current step's values before proceeding to the
   * next step.
   *
   * Overrides Drupal\Core\Entity\EntityFormController::submit().
   */
  public function submit(array $form, array &$form_state) {
    // Build the node object from the submitted values.
    $rota = parent::submit($form, $form_state);

    return $rota;
  }


  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $rota = $this->entity;
    $insert = $rota->isNew();
    $rota->save();
    $rota_link = l(t('view'), 'node/' . $rota->id());
    $watchdog_args = array('%title' => $rota->label());
    $t_args = array('%title' => $rota->label());

    if ($insert) {
      watchdog('party_rota', 'Added Rota - %title.', $watchdog_args, WATCHDOG_NOTICE, $rota_link);
      drupal_set_message(t('Rota %title has been created.', $t_args));
    }
    else {
      watchdog('party_rota', 'Updated Rota - %title.', $watchdog_args, WATCHDOG_NOTICE, $rota_link);
      drupal_set_message(t('Rota %title has been updated.', $t_args));
    }

    if ($rota->id()) {
      $form_state['values']['rota_id'] = $rota->id();
      $form_state['$rota_id'] = $rota->id();
      if ($rota->access('view')) {
        $form_state['redirect_route'] = array(
          'route_name' => 'party_rota.edit',
          'route_parameters' => array(
            'rota' => $rota->id(),
          ),
        );
      }
      else {
        $form_state['redirect_route']['route_name'] = '<front>';
      }
    }
    else {
      // In the unlikely case something went wrong on save, the node will be
      // rebuilt and node form redisplayed the same way as in preview.
      drupal_set_message(t('The rota could not be saved.'), 'error');
      $form_state['rebuild'] = TRUE;
    }

    // Clear the page and block caches.
    cache_invalidate_tags(array('content' => TRUE));
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::delete().
   */
  public function delete(array $form, array &$form_state) {
    $destination = array();
    $query = \Drupal::request()->query;
    if ($query->has('destination')) {
      $destination = drupal_get_destination();
      $query->remove('destination');
    }
    $form_state['redirect_route'] = array(
      'route_name' => 'node.delete_confirm',
      'route_parameters' => array(
        'node' => $this->entity->id(),
      ),
      'options' => array(
        'query' => $destination,
      ),
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   */
  public function assign(array $form, array &$form_state, $rota, $rota_position, $rota_slot) {

  }

}

