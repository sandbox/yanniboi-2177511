<?php

/**
 * @file
 * Definition of Drupal\party_rota\Entity\Slot.
 */

namespace Drupal\party_rota\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Field\FieldDefinition;



/**
 * Defines the slot entity class.
 *
 * @EntityType(
 *   id = "rota_slot",
 *   label = @Translation("Slot"),
 *   controllers = {
 *     "storage" = "Drupal\Core\Entity\FieldableDatabaseStorageController",
 *     "form" = {
 *       "default" = "Drupal\party_rota\SlotFormController",
 *     },
 *   },
 *   base_table = "party_rota_slot",
 *   entity_keys = {
 *     "id" = "slot_id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   }
 * )
 */

class Slot extends ContentEntityBase implements ContentEntityInterface {

  /**
   * Implements Drupal\Core\Entity\EntityInterface::id().
   */
  public function id() {
    return $this->get('slot_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageControllerInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller,$values);

    if (empty($values['created'])) {
      $values['created'] = REQUEST_TIME;
    }
  }

  public function preSave(EntityStorageControllerInterface $storage_controller) {
    parent::preSave($storage_controller);

    // Before saving the node, set changed and revision times.
    $this->changed->value = REQUEST_TIME;
  }

  public function access($operation = 'view', AccountInterface $account = NULL) {
    if ($operation == 'create') {
      return parent::access($operation, $account);
    }
    else {
      // @TODO do access handling.
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }


  public function __construct($values = array()) {
    parent::__construct($values, 'rota_slot');
  }

  protected function defaultLabel() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions($entity_type) {
    $fields['slot_id'] = FieldDefinition::create('integer')
      ->setLabel(t('Slot ID'))
      ->setDescription(t('The slot ID.'))
      ->setReadOnly(TRUE);

    $fields['rota_id'] = FieldDefinition::create('integer')
      ->setLabel(t('Rota ID'))
      ->setDescription(t('The rota ID.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = FieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The rota UUID.'))
      ->setReadOnly(TRUE);

    $fields['language'] = FieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The rota language code.'));

    $fields['title'] = FieldDefinition::create('text')
      // @todo Account for $rota_type->title_label when per-bundle overrides are
      //   possible - https://drupal.org/rota/2114707.
      ->setLabel(t('Title'))
      ->setDescription(t('The title of this rota, always treated as non-markup plain text.'))
      ->setClass('Drupal\Core\Field\FieldItemList')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = FieldDefinition::create('text')
      // @todo Account for $rota_type->title_label when per-bundle overrides are
      //   possible - https://drupal.org/rota/2114707.
      ->setLabel(t('Description'))
      ->setDescription(t('The title of this rota, always treated as non-markup plain text.'))
      ->setClass('Drupal\Core\Field\FieldItemList')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    // @todo Convert to a "created" field in https://drupal.org/node/2145103.
    $fields['date'] = FieldDefinition::create('integer')
      ->setLabel(t('Date'))
      ->setDescription(t('The time that the entity will happen.'));

    // @todo Convert to a "created" field in https://drupal.org/node/2145103.
    $fields['created'] = FieldDefinition::create('integer')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    // @todo Convert to a "changed" field in https://drupal.org/node/2145103.
    $fields['changed'] = FieldDefinition::create('integer')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'))
      ->setPropertyConstraints('value', array('EntityChanged' => array()));

    return $fields;
  }


}
