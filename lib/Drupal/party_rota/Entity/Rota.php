<?php

/**
 * @file
 * Definition of Drupal\party_rota\Entity\Rota.
 */

namespace Drupal\party_rota\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Field\FieldDefinition;



/**
 * Defines the rota entity class.
 *
 * @EntityType(
 *   id = "rota",
 *   label = @Translation("Rota"),
 *   controllers = {
 *     "storage" = "Drupal\Core\Entity\FieldableDatabaseStorageController",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessController",
 *     "form" = {
 *       "default" = "Drupal\party_rota\RotaFormController",
 *       "delete" = "Drupal\Core\Entity\ContentEntityFormController",
 *       "edit" = "Drupal\party_rota\RotaFormController"
 *     },
 *     "list" = "Drupal\Core\Entity\EntityListController",
 *     "translation" = "Drupal\content_translation\ContentTranslationController"
 *   },
 *   base_table = "party_rota",
 *   uri_callback = "party_rota_uri",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   render_cache = FALSE,
 *   entity_keys = {
 *     "id" = "rota_id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "rota.view",
 *     "edit-form" = "rota.edit",
 *   }
 * )
 */

class Rota extends ContentEntityBase implements ContentEntityInterface {

  /**
   * Implements Drupal\Core\Entity\EntityInterface::id().
   */
  public function id() {
    return $this->get('rota_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageControllerInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller,$values);

    if (empty($values['created'])) {
      $values['created'] = REQUEST_TIME;
    }
  }

  public function preSave(EntityStorageControllerInterface $storage_controller) {
    parent::preSave($storage_controller);

    // Before saving the node, set changed and revision times.
    $this->set('changed', 'Test');
    $this->changed->value = REQUEST_TIME;
  }

  public function access($operation = 'view', AccountInterface $account = NULL) {
    if ($operation == 'create') {
      return parent::access($operation, $account);
    }
    else {
      // @TODO do access handling.
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions($entity_type) {
    $fields['rota_id'] = FieldDefinition::create('integer')
      ->setLabel(t('Rota ID'))
      ->setDescription(t('The rota ID.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = FieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The rota UUID.'))
      ->setReadOnly(TRUE);

    $fields['language'] = FieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The rota language code.'));

    $fields['title'] = FieldDefinition::create('text')
      // @todo Account for $rota_type->title_label when per-bundle overrides are
      //   possible - https://drupal.org/rota/2114707.
      ->setLabel(t('Title'))
      ->setDescription(t('The title of this rota, always treated as non-markup plain text.'))
      ->setClass('Drupal\Core\Field\FieldItemList')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    // @todo Convert to a "created" field in https://drupal.org/node/2145103.
    $fields['created'] = FieldDefinition::create('integer')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    // @todo Convert to a "changed" field in https://drupal.org/node/2145103.
    $fields['changed'] = FieldDefinition::create('integer')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'))
      ->setPropertyConstraints('value', array('EntityChanged' => array()));

    return $fields;
  }


  public function __construct($values = array()) {
    parent::__construct($values, 'rota');
  }

  protected function defaultLabel() {
    return $this->label();
  }

  protected function defaultUri() {
    return array('path' => 'rota/' . $this->id());
  }

  /**
   * Fetches a list of positions attached to a Rota.
   */
  public function getSlots() {
    $result = db_select('party_rota_slot', 's')
      ->fields('s')
      //->condition('rota_id', $this->get('rota_id')->getValue(), '=')
      ->condition('rota_id', $this->id(), '=')
      ->execute()
      ->fetchAll();

    if (isset($result)) {
      $positions = array();

      foreach ($result as $data) {
        $positions[$data->slot_id] = entity_load('rota_slot', $data->slot_id);
      }

      return $positions;
    }

    return FALSE;
  }

  /**
   * Fetches a list of positions attached to a Rota.
   */
  public function getPositions() {
    $result = db_select('party_rota_position', 'p')
      ->fields('p')
      ->condition('rota_id', $this->id(), '=')
      ->execute()
      ->fetchAll();

    if (isset($result)) {
      $positions = array();

      foreach ($result as $data) {
        $positions[$data->position_id] = entity_load('rota_position', $data->position_id);
      }

      return $positions;
    }

    return FALSE;
  }

  /**
   * Fetches a list of positions attached to a Rota.
   */
  public function getAssignmentFromContext($position_id, $slot_id) {
    $result = db_select('party_rota_assignment', 'a')
      ->fields('a')
      ->condition('rota_id', $this->id(), '=')
      ->condition('position_id', $position_id, '=')
      ->condition('slot_id', $slot_id, '=')
      ->execute()
      ->fetchAll();

    if (isset($result)) {
      return reset($result);
    }

    return FALSE;
  }

}
